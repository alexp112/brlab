<?php

namespace App\Command;

use App\Service\GamesDataProcessingService;
use App\Service\GetGamesDataService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Команда для получения данных о матчах от поставщиков и сохранения в БД
 */
class GetDataCommand extends Command
{
    protected static $defaultName = 'app:get-data';

    /**
     * Сервис получения данных об играх от поставщиков данных
     *
     * @var GetGamesDataService
     */
    private $getGamesDataService;
    /**
     * Сервис обработки и сохранения данных о матчах
     *
     * @var GamesDataProcessingService
     */
    private $gameDataProcessingService;

    /**
     * GetDataCommand constructor.
     *
     * @param GetGamesDataService $getGamesDataService
     * @param GamesDataProcessingService $gamesDataProcessingService
     */
    public function __construct(
        GetGamesDataService $getGamesDataService,
        GamesDataProcessingService $gamesDataProcessingService
    ) {
        parent::__construct();
        $this->getGamesDataService = $getGamesDataService;
        $this->gameDataProcessingService = $gamesDataProcessingService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Получает данные о матчах от поставщиков, обрабатывает и сохраняет в БД.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $gamesDataDTO = $this->getGamesDataService->getData();
        $result = $this->gameDataProcessingService->process($gamesDataDTO);
        $output->writeln(
            sprintf(
                'Количество обработанных матчей: %s',
                $result->getGamesProcessedNum()
            )
        );
        $output->writeln(
            sprintf(
                'Количество добавленных буферных матчей: %s',
                $result->getBufferGamesInsertedNum()
            )
        );
        $output->writeln(
            sprintf(
                'Количество добавленных матчей: %s',
                $result->getGamesInsertedNum()
            )
        );
    }
}