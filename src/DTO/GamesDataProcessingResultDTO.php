<?php

namespace App\DTO;

/**
 * DTO для данных об обработанных играх
 */
final class GamesDataProcessingResultDTO
{
    /**
     * Кол-во обработанных игр
     *
     * @var int
     */
    private $gamesProcessedNum = 0;
    /**
     * Кол-во сохранённых буферных игр
     *
     * @var int
     */
    private $bufferGamesInsertedNum = 0;
    /**
     * Кол-во сохранённых игр
     *
     * @var int
     */
    private $gamesInsertedNum = 0;

    /**
     * @return int
     */
    public function getGamesProcessedNum(): int
    {
        return $this->gamesProcessedNum;
    }

    /**
     * @param int $gamesProcessedNum
     */
    public function setGamesProcessedNum(int $gamesProcessedNum): void
    {
        $this->gamesProcessedNum = $gamesProcessedNum;
    }

    /**
     * Добавляет одну обработанную игру
     */
    public function incrementOneProcessedGame(): void
    {
        $this->gamesProcessedNum += 1;
    }

    /**
     * @return int
     */
    public function getBufferGamesInsertedNum(): int
    {
        return $this->bufferGamesInsertedNum;
    }

    /**
     * @param int $bufferGamesInsertedNum
     */
    public function setBufferGamesInsertedNum(int $bufferGamesInsertedNum): void
    {
        $this->bufferGamesInsertedNum = $bufferGamesInsertedNum;
    }

    /**
     * Добавляет одну буферную игру
     */
    public function incrementOneInsertedBufferGame(): void
    {
        $this->bufferGamesInsertedNum += 1;
    }

    /**
     * @return int
     */
    public function getGamesInsertedNum(): int
    {
        return $this->gamesInsertedNum;
    }

    /**
     * @param int $gamesInsertedNum
     */
    public function setGamesInsertedNum(int $gamesInsertedNum): void
    {
        $this->gamesInsertedNum = $gamesInsertedNum;
    }

    /**
     * Добавляет одну игру
     */
    public function incrementOneInsertedGame(): void
    {
        $this->gamesInsertedNum += 1;
    }
}
