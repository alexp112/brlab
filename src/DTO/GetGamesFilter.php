<?php

namespace App\DTO;

/**
 * DTO для получения данных об играх
 */
final class GetGamesFilter
{
    /**
     * Название источника
     *
     * @var string|null
     */
    private $source;

    /**
     * Начало интервала даты матча
     *
     * @var \DateTime|null
     */
    private $matchStartAtFrom;

    /**
     * Конец интервала даты матча
     *
     * @var \DateTime|null
     */
    private $matchStartAtTo;

    public function __construct(
        ?string $source,
        ?string $matchStartAtFrom,
        ?string $matchStartAtTo
    )
    {
        if (!empty($source)) {
            $this->source = $source;
        }

        if (!empty($matchStartAtFrom)) {
            $this->matchStartAtFrom = \DateTime::createFromFormat('Y-m-d H:i:s', $matchStartAtFrom) ?: null;
        }

        if (!empty($matchStartAtTo)) {
            $this->matchStartAtTo = \DateTime::createFromFormat('Y-m-d H:i:s', $matchStartAtTo) ?: null;
        }
    }

    /**
     * @return null|string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @return \DateTime|null
     */
    public function getMatchStartAtFrom(): ?\DateTime
    {
        return $this->matchStartAtFrom;
    }

    /**
     * @return \DateTime|null
     */
    public function getMatchStartAtTo(): ?\DateTime
    {
        return $this->matchStartAtTo;
    }
}
