<?php

namespace App\DTO;

/**
 * DTO для конкретной игры
 */
final class GameDTO
{
    /**
     * Дата проведения игры
     *
     * @var \DateTime
     */
    private $date;
    /**
     * Команда 1
     *
     * @var string
     */
    private $team1;
    /**
     * Команда 2
     *
     * @var string
     */
    private $team2;

    /**
     * GameDTO constructor.
     *
     * @param \DateTime $date
     * @param string $team1
     * @param string $team2
     */
    public function __construct(\DateTime $date, string $team1, string $team2)
    {
        $this->date = $date;
        $this->setTeam1(trim($team1));
        $this->setTeam2(trim($team2));
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getTeam1(): string
    {
        return $this->team1;
    }

    /**
     * @param string $team1
     */
    public function setTeam1(string $team1): void
    {
        $this->team1 = $team1;
    }

    /**
     * @return string
     */
    public function getTeam2(): string
    {
        return $this->team2;
    }

    /**
     * @param string $team2
     */
    public function setTeam2(string $team2): void
    {
        $this->team2 = $team2;
    }


}