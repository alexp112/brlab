<?php

namespace App\DTO;

/**
 * DTO для данных об играх
 */
final class GamesDataDTO
{
    /**
     * Источник данных
     *
     * @var string
     */
    private $source;
    /**
     * Язык
     *
     * @var string
     */
    private $language;
    /**
     * Вид спорта
     *
     * @var string
     */
    private $sportType;
    /**
     * Лига
     *
     * @var string
     */
    private $league;
    /**
     * Массив игр
     *
     * @var GameDTO[]
     */
    private $games = [];

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = trim($source);
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = trim($language);
    }

    /**
     * @return string
     */
    public function getSportType(): string
    {
        return $this->sportType;
    }

    /**
     * @param string $sportType
     */
    public function setSportType(string $sportType): void
    {
        $this->sportType = trim($sportType);
    }

    /**
     * @return string
     */
    public function getLeague(): string
    {
        return $this->league;
    }

    /**
     * @param string $league
     */
    public function setLeague(string $league): void
    {
        $this->league = trim($league);
    }

    /**
     * @return GameDTO[]
     */
    public function getGames(): array
    {
        return $this->games;
    }

    /**
     * @param GameDTO[] $games
     */
    public function setGames(array $games): void
    {
        $this->games = $games;
    }

    /**
     * @param GameDTO $game
     */
    public function addGame(GameDTO $game): void
    {
        $this->games[] = $game;
    }


}