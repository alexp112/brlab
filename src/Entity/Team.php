<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Сущность команды
 *
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * Идентификатор команды
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"all"})
     */
    private $id;

    /**
     * Название команды
     *
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"all"})
     */
    private $name;

    /**
     * Связанный вид спорта
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SportType", inversedBy="teams")
     */
    private $sportType;

    /**
     * Связанный словарь возможных значений названий команды
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TeamDictionary", mappedBy="team")
     */
    private $teamDictionaries;

    public function __construct()
    {
        $this->teamDictionaries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSportType(): ?SportType
    {
        return $this->sportType;
    }

    public function setSportType(?SportType $sportType): self
    {
        $this->sportType = $sportType;

        return $this;
    }

    /**
     * @return Collection|TeamDictionary[]
     */
    public function getTeamDictionaries(): Collection
    {
        return $this->teamDictionaries;
    }

    public function addTeamDictionary(TeamDictionary $teamDictionary): self
    {
        if (!$this->teamDictionaries->contains($teamDictionary)) {
            $this->teamDictionaries[] = $teamDictionary;
            $teamDictionary->setTeam($this);
        }

        return $this;
    }

    public function removeTeamDictionary(TeamDictionary $teamDictionary): self
    {
        if ($this->teamDictionaries->contains($teamDictionary)) {
            $this->teamDictionaries->removeElement($teamDictionary);
            // set the owning side to null (unless already changed)
            if ($teamDictionary->getTeam() === $this) {
                $teamDictionary->setTeam(null);
            }
        }

        return $this;
    }
}
