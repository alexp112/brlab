<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Сущность вида спорта
 *
 * @ORM\Entity(repositoryClass="App\Repository\SportTypeRepository")
 */
class SportType
{
    /**
     * Идентификатор вида спорта
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"all"})
     */
    private $id;

    /**
     * Название вида спорта
     *
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"all"})
     */
    private $name;

    /**
     * Словарь связанных возможных значений названия вида спорта
     *
     * @ORM\OneToMany(targetEntity="App\Entity\SportTypeDictionary", mappedBy="sportType")
     */
    private $sportTypeDictionaries;

    /**
     * Связанные лиги
     *
     * @ORM\OneToMany(targetEntity="App\Entity\League", mappedBy="sportType")
     */
    private $leagues;

    /**
     * Связанные команды
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Team", mappedBy="sportType")
     */
    private $teams;

    public function __construct()
    {
        $this->sportTypeDictionaries = new ArrayCollection();
        $this->leagues = new ArrayCollection();
        $this->teams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|SportTypeDictionary[]
     */
    public function getSportTypeDictionaries(): Collection
    {
        return $this->sportTypeDictionaries;
    }

    public function addSportTypeDictionary(SportTypeDictionary $sportTypeDictionary): self
    {
        if (!$this->sportTypeDictionaries->contains($sportTypeDictionary)) {
            $this->sportTypeDictionaries[] = $sportTypeDictionary;
            $sportTypeDictionary->setSportType($this);
        }

        return $this;
    }

    public function removeSportTypeDictionary(SportTypeDictionary $sportTypeDictionary): self
    {
        if ($this->sportTypeDictionaries->contains($sportTypeDictionary)) {
            $this->sportTypeDictionaries->removeElement($sportTypeDictionary);
            // set the owning side to null (unless already changed)
            if ($sportTypeDictionary->getSportType() === $this) {
                $sportTypeDictionary->setSportType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|League[]
     */
    public function getLeagues(): Collection
    {
        return $this->leagues;
    }

    public function addLeague(League $league): self
    {
        if (!$this->leagues->contains($league)) {
            $this->leagues[] = $league;
            $league->setSportType($this);
        }

        return $this;
    }

    public function removeLeague(League $league): self
    {
        if ($this->leagues->contains($league)) {
            $this->leagues->removeElement($league);
            // set the owning side to null (unless already changed)
            if ($league->getSportType() === $this) {
                $league->setSportType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->setSportType($this);
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
            // set the owning side to null (unless already changed)
            if ($team->getSportType() === $this) {
                $team->setSportType(null);
            }
        }

        return $this;
    }
}
