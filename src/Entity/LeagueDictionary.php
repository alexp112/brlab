<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Словарь названий лиг (список возможных значений названий лиг)
 *
 * @ORM\Entity(repositoryClass="App\Repository\LeagueDictionaryRepository")
 */
class LeagueDictionary
{
    /**
     * Идентификатор словаря лиги
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Связанная лига
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\League", inversedBy="leagueDictionaries")
     */
    private $league;

    /**
     * Возможное значение названия лиги
     *
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLeague(): ?League
    {
        return $this->league;
    }

    public function setLeague(?League $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
