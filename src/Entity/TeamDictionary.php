<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сущность возможных значений названий команды
 *
 * @ORM\Entity(repositoryClass="App\Repository\TeamDictionaryRepository")
 */
class TeamDictionary
{
    /**
     * Идентификатор возможного значения названий команды
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Связанная команда
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="teamDictionaries")
     */
    private $team;

    /**
     * Возможное значение названия команды
     *
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
