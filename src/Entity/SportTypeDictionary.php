<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Словарь возможных значений названий вида спорта
 *
 * @ORM\Entity(repositoryClass="App\Repository\SportTypeDictionaryRepository")
 */
class SportTypeDictionary
{
    /**
     * Идентификаторв возможного значения названия вида спорта
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Связанный вид спорта
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SportType", inversedBy="sportTypeDictionaries")
     */
    private $sportType;

    /**
     * Возможное значение названия вида спорта
     *
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSportType(): ?SportType
    {
        return $this->sportType;
    }

    public function setSportType(?SportType $sportType): self
    {
        $this->sportType = $sportType;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
