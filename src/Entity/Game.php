<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Сущность игры
 *
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Game
{
    /**
     * Идентификатор игры
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"all"})
     */
    private $id;

    /**
     * Язык
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"all"})
     */
    private $language;

    /**
     * Вид спорта
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SportType")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"all"})
     */
    private $sportType;

    /**
     * Лига
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\League")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"all"})
     */
    private $league;

    /**
     * Команда 1
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Team")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"all"})
     */
    private $team1;

    /**
     * Команда 2
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Team")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"all"})
     */
    private $team2;

    /**
     * Дата и время начала матча
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"all"})
     * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $matchStartAt;

    /**
     * Буферные игры, связанные с этой игрой
     *
     * @ORM\OneToMany(targetEntity="App\Entity\GameBuffer", mappedBy="game")
     */
    private $gameBuffers;

    /**
     * Кол-во буферных игр, связанных с этой игрой
     *
     * @var int
     */
    private $gameBuffersCountValue;

    /**
     * Дата и время добавления сущности в БД
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->gameBuffers = new ArrayCollection();
    }

    /**
     * Возвращает кол-во буферных матчей, или получает их из БД, если не было установлено
     *
     * @JMS\Groups({"all"})
     * @JMS\VirtualProperty()
     */
    public function getGameBuffersCount()
    {
        if ($this->gameBuffersCountValue === null) {
            $this->gameBuffersCountValue = $this->gameBuffers->count();
        }
        return $this->gameBuffersCountValue;
    }

    /**
     * @return int
     */
    public function getGameBuffersCountValue(): int
    {
        return $this->gameBuffersCountValue;
    }

    /**
     * @param int $gameBuffersCountValue
     */
    public function setGameBuffersCountValue(int $gameBuffersCountValue): void
    {
        $this->gameBuffersCountValue = $gameBuffersCountValue;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateCreatedAt(): void
    {
        $this->setCreatedAt(new \DateTime('now'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getSportType(): ?SportType
    {
        return $this->sportType;
    }

    public function setSportType(?SportType $sportType): self
    {
        $this->sportType = $sportType;

        return $this;
    }

    public function getLeague(): ?League
    {
        return $this->league;
    }

    public function setLeague(?League $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getTeam1(): ?Team
    {
        return $this->team1;
    }

    public function setTeam1(?Team $team1): self
    {
        $this->team1 = $team1;

        return $this;
    }

    public function getTeam2(): ?Team
    {
        return $this->team2;
    }

    public function setTeam2(?Team $team2): self
    {
        $this->team2 = $team2;

        return $this;
    }

    public function getMatchStartAt(): ?\DateTimeInterface
    {
        return $this->matchStartAt;
    }

    public function setMatchStartAt(\DateTimeInterface $matchStartAt): self
    {
        $this->matchStartAt = $matchStartAt;

        return $this;
    }

    /**
     * @return Collection|GameBuffer[]
     */
    public function getGameBuffers(): Collection
    {
        return $this->gameBuffers;
    }

    public function addGameBuffer(GameBuffer $gameBuffer): self
    {
        if (!$this->gameBuffers->contains($gameBuffer)) {
            $this->gameBuffers[] = $gameBuffer;
            $gameBuffer->setGame($this);
        }

        return $this;
    }

    public function removeGameBuffer(GameBuffer $gameBuffer): self
    {
        if ($this->gameBuffers->contains($gameBuffer)) {
            $this->gameBuffers->removeElement($gameBuffer);
            // set the owning side to null (unless already changed)
            if ($gameBuffer->getGame() === $this) {
                $gameBuffer->setGame(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function toArray(): array
    {
        $out = ['id' => $this->getId()];
        $out['languageId'] = $this->getLanguage() === null ? null : $this->getLanguage()->getId();
        $out['sportTypeId'] = $this->getSportType() === null ? null : $this->getSportType()->getId();
        $out['leagueId'] = $this->getLeague() === null ? null : $this->getLeague()->getId();
        $out['team1Id'] = $this->getTeam1() === null ? null : $this->getTeam1()->getId();
        $out['team2Id'] = $this->getTeam2() === null ? null : $this->getTeam2()->getId();
        $out['matchStartAt'] = $this->getMatchStartAt() === null ? null : $this->getMatchStartAt()->format('Y.m.d H:i:s');
        $out['createdAt'] = $this->getCreatedAt() === null ? null : $this->getCreatedAt()->format('Y.m.d H:i:s');
        return $out;
    }
}
