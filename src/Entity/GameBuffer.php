<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сущность буферного матча
 *
 * @ORM\Entity(repositoryClass="App\Repository\GameBufferRepository")
 * @ORM\HasLifecycleCallbacks
 */
class GameBuffer
{
    /**
     * Идентификатор буферного матча
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Язык
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     */
    private $language;

    /**
     * Вид спорта
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SportType")
     */
    private $sportType;

    /**
     * Лига
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\League")
     */
    private $league;

    /**
     * Команда 1
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Team")
     */
    private $team1;

    /**
     * Команда 2
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Team")
     */
    private $team2;

    /**
     * Дата и время начала матча
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $matchStartAt;

    /**
     * Источник данных
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Source")
     * @ORM\JoinColumn(nullable=false)
     */
    private $source;

    /**
     * Дата и время добавления сущности в БД
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * Связанная игра
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Game", inversedBy="gameBuffers")
     * @ORM\JoinColumn(nullable=true)
     */
    private $game;

    /**
     * @ORM\PrePersist
     */
    public function updateCreatedAt(): void
    {
        $this->setCreatedAt(new \DateTime('now'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getSportType(): ?SportType
    {
        return $this->sportType;
    }

    public function setSportType(?SportType $sportType): self
    {
        $this->sportType = $sportType;

        return $this;
    }

    public function getLeague(): ?League
    {
        return $this->league;
    }

    public function setLeague(?League $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getTeam1(): ?Team
    {
        return $this->team1;
    }

    public function setTeam1(?Team $team1): self
    {
        $this->team1 = $team1;

        return $this;
    }

    public function getTeam2(): ?Team
    {
        return $this->team2;
    }

    public function setTeam2(?Team $team2): self
    {
        $this->team2 = $team2;

        return $this;
    }

    public function getMatchStartAt(): ?\DateTimeInterface
    {
        return $this->matchStartAt;
    }

    public function setMatchStartAt(?\DateTimeInterface $matchStartAt): self
    {
        $this->matchStartAt = $matchStartAt;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function toArray(): array
    {
        $out = ['id' => $this->getId()];
        $out['languageId'] = $this->getLanguage() === null ? null : $this->getLanguage()->getId();
        $out['sportTypeId'] = $this->getSportType() === null ? null : $this->getSportType()->getId();
        $out['leagueId'] = $this->getLeague() === null ? null : $this->getLeague()->getId();
        $out['team1Id'] = $this->getTeam1() === null ? null : $this->getTeam1()->getId();
        $out['team2Id'] = $this->getTeam2() === null ? null : $this->getTeam2()->getId();
        $out['matchStartAt'] = $this->getMatchStartAt() === null ? null : $this->getMatchStartAt()->format('Y.m.d H:i:s');
        $out['sourceId'] = $this->getSource() === null ? null : $this->getSource()->getId();
        $out['createdAt'] = $this->getCreatedAt() === null ? null : $this->getCreatedAt()->format('Y.m.d H:i:s');
        $out['gameId'] = $this->getGame() === null ? null : $this->getGame()->getId();
        return $out;
    }
}
