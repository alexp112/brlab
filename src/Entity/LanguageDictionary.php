<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сущность словаря языка (возможные значения)
 *
 * @ORM\Entity(repositoryClass="App\Repository\LanguageDictionaryRepository")
 */
class LanguageDictionary
{
    /**
     * Идентификатор словаря языка
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Связанный язык
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="languageDictionaries")
     */
    private $language;

    /**
     * Значение словаря языка
     *
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
