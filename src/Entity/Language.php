<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Сущность языка.
 *
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 */
class Language
{
    /**
     * Идентификатор языка
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"all"})
     */
    private $id;

    /**
     * Название языка
     *
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"all"})
     */
    private $name;

    /**
     * Код языка
     *
     * @ORM\Column(type="string", length=10)
     */
    private $code;

    /**
     * Возможные значения языка, связанные с данным языком
     *
     * @ORM\OneToMany(targetEntity="App\Entity\LanguageDictionary", mappedBy="language")
     */
    private $languageDictionaries;

    public function __construct()
    {
        $this->languageDictionaries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|LanguageDictionary[]
     */
    public function getLanguageDictionaries(): Collection
    {
        return $this->languageDictionaries;
    }

    public function addLanguageDictionary(LanguageDictionary $languageDictionary): self
    {
        if (!$this->languageDictionaries->contains($languageDictionary)) {
            $this->languageDictionaries[] = $languageDictionary;
            $languageDictionary->setLanguage($this);
        }

        return $this;
    }

    public function removeLanguageDictionary(LanguageDictionary $languageDictionary): self
    {
        if ($this->languageDictionaries->contains($languageDictionary)) {
            $this->languageDictionaries->removeElement($languageDictionary);
            // set the owning side to null (unless already changed)
            if ($languageDictionary->getLanguage() === $this) {
                $languageDictionary->setLanguage(null);
            }
        }

        return $this;
    }
}
