<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Сущность лиги.
 *
 * @ORM\Entity(repositoryClass="App\Repository\LeagueRepository")
 */
class League
{
    /**
     * Идентификатор лиги
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"all"})
     */
    private $id;

    /**
     * Название лиги
     *
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"all"})
     */
    private $name;

    /**
     * Список возможных значений названия лиги
     *
     * @ORM\OneToMany(targetEntity="App\Entity\LeagueDictionary", mappedBy="league")
     */
    private $leagueDictionaries;

    /**
     * Связанный вид спорта
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SportType", inversedBy="leagues")
     */
    private $sportType;

    public function __construct()
    {
        $this->leagueDictionaries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|LeagueDictionary[]
     */
    public function getLeagueDictionaries(): Collection
    {
        return $this->leagueDictionaries;
    }

    public function addLeagueDictionary(LeagueDictionary $leagueDictionary): self
    {
        if (!$this->leagueDictionaries->contains($leagueDictionary)) {
            $this->leagueDictionaries[] = $leagueDictionary;
            $leagueDictionary->setLeague($this);
        }

        return $this;
    }

    public function removeLeagueDictionary(LeagueDictionary $leagueDictionary): self
    {
        if ($this->leagueDictionaries->contains($leagueDictionary)) {
            $this->leagueDictionaries->removeElement($leagueDictionary);
            // set the owning side to null (unless already changed)
            if ($leagueDictionary->getLeague() === $this) {
                $leagueDictionary->setLeague(null);
            }
        }

        return $this;
    }

    public function getSportType(): ?SportType
    {
        return $this->sportType;
    }

    public function setSportType(?SportType $sportType): self
    {
        $this->sportType = $sportType;

        return $this;
    }
}
