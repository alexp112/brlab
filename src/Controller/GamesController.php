<?php

namespace App\Controller;

use App\DTO\GetGamesFilter;
use App\Entity\Game;
use App\Repository\GameRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Validator\Constraints;


/**
 * Контроллер для API игр
 */
class GamesController extends AbstractFOSRestController
{
    /**
     * Возвращает информацию об играх
     *
     * @FOSRest\Get("/api/games")
     * @FOSRest\View(serializerGroups={"all"})
     * @FOSRest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default="10",
     *     description="Количество возвращаемых игр")
     * @FOSRest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default="0",
     *     description="Смещение первой возвращаемой игры")
     * @FOSRest\QueryParam(
     *     name="source",
     *     description="Название источника")
     * @FOSRest\QueryParam(
     *     name="match_start_at_from",
     *     requirements=@Constraints\DateTime(format="Y-m-d H:i:s"),
     *     description="Начало интервала даты матча")
     * @FOSRest\QueryParam(
     *     name="match_start_at_to",
     *     requirements=@Constraints\DateTime(format="Y-m-d H:i:s"),
     *     description="Конец интервала даты матча")
     * @param ParamFetcher $paramFetcher
     * @return Game[]|array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getGames(ParamFetcher $paramFetcher)
    {
        $getGamesFilter = new GetGamesFilter(
            $paramFetcher->get('source'),
            $paramFetcher->get('match_start_at_from'),
            $paramFetcher->get('match_start_at_to')
        );

        /** @var GameRepository $gameRepository */
        $gameRepository = $this->getDoctrine()->getRepository(Game::class);
        $games = $gameRepository->getGames(
            $getGamesFilter,
            (int) $paramFetcher->get('limit'),
            (int) $paramFetcher->get('offset')
        );

        return $games;
    }
}