<?php

namespace App\Repository;

use App\Entity\SportTypeDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Репозиторий словарей видов спорта
 *
 * @method SportTypeDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method SportTypeDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method SportTypeDictionary[]    findAll()
 * @method SportTypeDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SportTypeDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SportTypeDictionary::class);
    }
}
