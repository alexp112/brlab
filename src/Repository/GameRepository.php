<?php

namespace App\Repository;

use App\DTO\GetGamesFilter;
use App\Entity\Game;
use App\Entity\GameBuffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Репозиторй матчей
 *
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }


    /**
     * Возвращает игру из таблицы game соответсвующую $gameBuffer
     * или null, если такого матча в game нет
     *
     * @param $gameBuffer GameBuffer
     *
     * @return Game|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByGameBuffer(GameBuffer $gameBuffer): ?Game
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.language = :language')->setParameter('language', $gameBuffer->getLanguage())
            ->andWhere('g.sportType = :sportType')->setParameter('sportType', $gameBuffer->getSportType())
            ->andWhere('g.league = :league')->setParameter('league', $gameBuffer->getLeague())
            ->andWhere('((g.team1 = :team1 AND g.team2 = :team2) OR (g.team2 = :team1 AND g.team1 = :team2))')
                ->setParameter('team1', $gameBuffer->getTeam1())
                ->setParameter('team2', $gameBuffer->getTeam2())
            ->andWhere('g.matchStartAt >= :matchStartAtBegin')->setParameter('matchStartAtBegin', (clone $gameBuffer->getMatchStartAt())->modify('-26 hours'))
            ->andWhere('g.matchStartAt <= :matchStartAtEnd')->setParameter('matchStartAtEnd', (clone $gameBuffer->getMatchStartAt())->modify('+26 hours'))
            ->orderBy('g.matchStartAt')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Возвращает список игр
     *
     * @param GetGamesFilter $getGamesFilter
     * @param int $limit
     * @param int $offset
     * @return Game[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getGames(
        GetGamesFilter $getGamesFilter,
        int $limit,
        int $offset
    ): array {
        $queryBuilder = $this->createQueryBuilder('g');

        if ($getGamesFilter->getSource() !== null) {
            $queryBuilder
                ->innerJoin('g.gameBuffers','gb')
                ->groupBy('gb.game')
                ->innerJoin('gb.source','s')
                ->andWhere('s.name LIKE :sourceName')
                ->setParameter('sourceName', '%' . $getGamesFilter->getSource() . '%');
        }

        if ($getGamesFilter->getMatchStartAtFrom() !== null) {
            $queryBuilder
                ->andWhere('g.matchStartAt >= :matchStartAtBegin')
                ->setParameter('matchStartAtBegin', $getGamesFilter->getMatchStartAtFrom());
        }

        if ($getGamesFilter->getMatchStartAtTo() !== null) {
            $queryBuilder
                ->andWhere('g.matchStartAt <= :matchStartAtEnd')
                ->setParameter('matchStartAtEnd', $getGamesFilter->getMatchStartAtTo());
        }

        $queryBuilder
            ->orderBy('g.matchStartAt')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $games = $queryBuilder
            ->getQuery()
            ->getResult();

        /** @var Game $game */
        foreach ($games as $game) {
            $this->countGameBuffers($game);
        }

        return $games;
    }

    /**
     * Вычисляет и устанавливает кол-во буферных матчей
     *
     * @param Game $game
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countGameBuffers(Game $game): void
    {
        $gameBuffersCount = $this->createQueryBuilder('g')
            ->innerJoin('g.gameBuffers','gb')
            ->andWhere('gb.game = :game')
            ->setParameter('game', $game)
            ->select('COUNT(gb.id) as gameBuffersCount')
            ->getQuery()
            ->getSingleScalarResult();
        $game->setGameBuffersCountValue($gameBuffersCount);
    }
}
