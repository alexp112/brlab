<?php

namespace App\Repository;

use App\Entity\TeamDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Репозиторий словарей команд
 *
 * @method TeamDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeamDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeamDictionary[]    findAll()
 * @method TeamDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeamDictionary::class);
    }
}
