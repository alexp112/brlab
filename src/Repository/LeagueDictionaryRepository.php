<?php

namespace App\Repository;

use App\Entity\LeagueDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Репозиторий словарей лиг
 *
 * @method LeagueDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeagueDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeagueDictionary[]    findAll()
 * @method LeagueDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeagueDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeagueDictionary::class);
    }
}
