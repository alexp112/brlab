<?php

namespace App\Repository;

use App\Entity\GameBuffer;
use App\Entity\Language;
use App\Entity\League;
use App\Entity\Source;
use App\Entity\SportType;
use App\Entity\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Репозиторй буферных матчей
 *
 * @method GameBuffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameBuffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameBuffer[]    findAll()
 * @method GameBuffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameBufferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameBuffer::class);
    }

    /**
     * Получает из БД буферный матч
     *
     * @param Language $language
     * @param SportType $sportType
     * @param League $league
     * @param Source $source
     * @param Team $team1
     * @param Team $team2
     * @param \DateTime $matchStartAt
     *
     * @return GameBuffer|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByAllParams(
        Language $language,
        SportType $sportType,
        League $league,
        Source $source,
        Team $team1,
        Team $team2,
        \DateTime $matchStartAt
    ): ?GameBuffer {

        return $this->createQueryBuilder('g')
            ->andWhere('g.language = :language')->setParameter('language', $language)
            ->andWhere('g.sportType = :sportType')->setParameter('sportType', $sportType)
            ->andWhere('g.league = :league')->setParameter('league', $league)
            ->andWhere('g.source = :source')->setParameter('source', $source)
            ->andWhere('((g.team1 = :team1 AND g.team2 = :team2) OR (g.team2 = :team1 AND g.team1 = :team2))')
                ->setParameter('team1', $team1)
                ->setParameter('team2', $team2)
            ->andWhere('g.matchStartAt >= :matchStartAtBegin')->setParameter('matchStartAtBegin', (clone $matchStartAt)->modify('-26 hours'))
            ->andWhere('g.matchStartAt <= :matchStartAtEnd')->setParameter('matchStartAtEnd', (clone $matchStartAt)->modify('+26 hours'))
            ->orderBy('g.matchStartAt')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
