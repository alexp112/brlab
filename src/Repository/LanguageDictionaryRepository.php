<?php

namespace App\Repository;

use App\Entity\LanguageDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Репозиторий словарей языков
 *
 * @method LanguageDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method LanguageDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method LanguageDictionary[]    findAll()
 * @method LanguageDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LanguageDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LanguageDictionary::class);
    }
}
