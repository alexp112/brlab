<?php

namespace App\Service;

use App\DTO\GameDTO;
use App\DTO\GamesDataDTO;
use App\DTO\GamesDataProcessingResultDTO;
use App\Entity\Game;
use App\Entity\GameBuffer;
use App\Repository\GameBufferRepository;
use App\Repository\GameRepository;
use App\Repository\LanguageDictionaryRepository;
use App\Repository\LanguageRepository;
use App\Repository\LeagueDictionaryRepository;
use App\Repository\LeagueRepository;
use App\Repository\SourceRepository;
use App\Repository\SportTypeDictionaryRepository;
use App\Repository\SportTypeRepository;
use App\Repository\TeamDictionaryRepository;
use App\Repository\TeamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerInterface;

/**
 * Сервис обработки и сохранения данных об играх от поставщиков
 */
class GamesDataProcessingService
{
    /**
     * Репозиторий игр
     *
     * @var GameRepository
     */
    private $gameRepository;
    /**
     * Репозиторий буферных матчей
     *
     * @var GameBufferRepository
     */
    private $gameBufferRepository;
    /**
     * Репозиторий словарей языков
     *
     * @var LanguageDictionaryRepository
     */
    private $languageDictionaryRepository;
    /**
     * Репозиторий языков
     *
     * @var LanguageRepository
     */
    private $languageRepository;
    /**
     * Репозиторий словарей лиг
     *
     * @var LeagueDictionaryRepository
     */
    private $leagueDictionaryRepository;
    /**
     * Репозиторий лиг
     *
     * @var LeagueRepository
     */
    private $leagueRepository;
    /**
     * Репозиторий источников данных
     *
     * @var SourceRepository
     */
    private $sourceRepository;
    /**
     * Репозиторий видов спорта
     *
     * @var SportTypeRepository
     */
    private $sportTypeRepository;
    /**
     * Репозиторий словарей видов спорта
     *
     * @var SportTypeDictionaryRepository
     */
    private $sportTypeDictionaryRepository;
    /**
     * Репозиторий словарей команд
     *
     * @var TeamDictionaryRepository
     */
    private $teamDictionaryRepository;
    /**
     * Репозиторий команд
     *
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * Менеджер сущностей
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * Служба логирования
     *
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(
        GameRepository $gameRepository,
        GameBufferRepository $gameBufferRepository,
        LanguageDictionaryRepository $languageDictionaryRepository,
        LanguageRepository $languageRepository,
        LeagueDictionaryRepository $leagueDictionaryRepository,
        LeagueRepository $leagueRepository,
        SourceRepository $sourceRepository,
        SportTypeRepository $sportTypeRepository,
        SportTypeDictionaryRepository $sportTypeDictionaryRepository,
        TeamDictionaryRepository $teamDictionaryRepository,
        TeamRepository $teamRepository,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->gameRepository = $gameRepository;
        $this->gameBufferRepository = $gameBufferRepository;
        $this->languageDictionaryRepository = $languageDictionaryRepository;
        $this->languageRepository = $languageRepository;
        $this->leagueDictionaryRepository = $leagueDictionaryRepository;
        $this->leagueRepository = $leagueRepository;
        $this->sourceRepository = $sourceRepository;
        $this->sportTypeRepository = $sportTypeRepository;
        $this->sportTypeDictionaryRepository = $sportTypeDictionaryRepository;
        $this->teamDictionaryRepository = $teamDictionaryRepository;
        $this->teamRepository = $teamRepository;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * Обрабатывает и сохраняет игры
     *
     * @param GamesDataDTO $gamesDataDTO
     *
     * @return GamesDataProcessingResultDTO
     */
    public function process(GamesDataDTO $gamesDataDTO): GamesDataProcessingResultDTO
    {
        $result = new GamesDataProcessingResultDTO();

        /* Получаем язык из БД */
        $languageDictionary = $this->languageDictionaryRepository->findOneBy(['value' => $gamesDataDTO->getLanguage()]);
        if ($languageDictionary === null || $languageDictionary->getLanguage() === null) {
            $this->logger->error(
                sprintf(
                    'Failed to process games data, language not found. Language value: "%s"',
                    $gamesDataDTO->getLanguage()
                )
            );
            return $result;
        }
        $language = $languageDictionary->getLanguage();

        /* Получаем источник из БД */
        $source = $this->sourceRepository->findOneBy(['name' => $gamesDataDTO->getSource()]);
        if ($source === null) {
            $this->logger->error(
                sprintf(
                    'Failed to process games data, source not found. Source value: "%s"',
                    $gamesDataDTO->getSource()
                )
            );
            return $result;
        }

        /* Получаем вид спорта из БД */
        $sportTypeDictionary = $this->sportTypeDictionaryRepository->findOneBy(['value' => $gamesDataDTO->getSportType()]);
        if ($sportTypeDictionary === null || $sportTypeDictionary->getSportType() === null) {
            $this->logger->error(
                sprintf(
                    'Failed to process games data, sport type not found. Value: "%s"',
                    $gamesDataDTO->getSportType()
                )
            );
            return $result;
        }
        $sportType = $sportTypeDictionary->getSportType();

        /* Получаем лигу из БД */
        $leagueDictionary = $this->leagueDictionaryRepository->findOneBy(['value' => $gamesDataDTO->getLeague()]);
        if ($leagueDictionary === null || $leagueDictionary->getLeague() === null) {
            $this->logger->error(
                sprintf(
                    'Failed to process games data, league not found. Value: "%s"',
                    $gamesDataDTO->getLeague()
                )
            );
            return $result;
        }
        $league = $leagueDictionary->getLeague();

        /* Обрабатываем игры */
        /** @var GameDTO $gameDTO */
        foreach ($gamesDataDTO->getGames() as $gameDTO)
        {
            $result->incrementOneProcessedGame();

            /* Получаем первую команду из БД */
            $team1Dictionary = $this->teamDictionaryRepository->findOneBy(['value' => $gameDTO->getTeam1()]);
            if ($team1Dictionary !== null) {
                $team1 = $team1Dictionary->getTeam();
            }
            if ($team1 === null) {
                $this->logger->warning(
                    sprintf(
                        'Team not found. Value: "%s"',
                        $gameDTO->getTeam1()
                    )
                );
            }

            /* Получаем вторую команду из БД */
            $team2Dictionary = $this->teamDictionaryRepository->findOneBy(['value' => $gameDTO->getTeam2()]);
            if ($team2Dictionary !== null) {
                $team2 = $team2Dictionary->getTeam();
            }
            if ($team2 === null) {
                $this->logger->warning(
                    sprintf(
                        'Team not found. Value: "%s"',
                        $gameDTO->getTeam2()
                    )
                );
            }

            /* Если обе команды найдены - продолжаем обработку */
            if ($team1 !== null && $team2 !== null) {
                $gameBuffer = null;
                /* Проверяем, есть ли буферный матч с такими данными */
                try {
                    $gameBuffer = $this->gameBufferRepository->findOneByAllParams(
                        $language,
                        $sportType,
                        $league,
                        $source,
                        $team1,
                        $team2,
                        $gameDTO->getDate()
                    );
                } catch (NonUniqueResultException $e) {
                    $this->logger->critical(
                        sprintf(
                            'Error while getting buffer game from DB. "%s".',
                            $e->getMessage()
                        )
                    );
                }

                /* Если буферного матча нет - добавляем его */
                if ($gameBuffer === null) {
                    $gameBuffer = new GameBuffer();
                    $gameBuffer->setSource($source);
                    $gameBuffer->setLanguage($language);
                    $gameBuffer->setSportType($sportType);
                    $gameBuffer->setLeague($league);
                    $gameBuffer->setTeam1($team1);
                    $gameBuffer->setTeam2($team2);
                    $gameBuffer->setMatchStartAt($gameDTO->getDate());
                    $this->entityManager->persist($gameBuffer);
                    $this->entityManager->flush();
                    $result->incrementOneInsertedBufferGame();
                }

                $this->logger->debug(
                    'GameBuffer debug info.',
                    $gameBuffer->toArray()
                );

                /* Проверяем, есть ли данный матч в таблице game */
                try {
                    $game = $this->gameRepository->findByGameBuffer($gameBuffer);
                } catch (NonUniqueResultException $e) {
                    $this->logger->critical(
                        sprintf(
                            'Error while getting game from DB. "%s".',
                            $e->getMessage()
                        ),
                        [
                            'gameBuffer' => $gameBuffer->toArray()
                        ]
                    );
                }

                /* Если игры в таблице game нет - создаём новую игру */
                if ($game === null) {
                    $game = new Game();
                    $game->setLanguage($gameBuffer->getLanguage());
                    $game->setSportType($gameBuffer->getSportType());
                    $game->setLeague($gameBuffer->getLeague());
                    $game->setTeam1($gameBuffer->getTeam1());
                    $game->setTeam2($gameBuffer->getTeam2());
                    $game->setMatchStartAt($gameBuffer->getMatchStartAt());
                    $this->entityManager->persist($game);
                    $this->entityManager->flush();
                    $result->incrementOneInsertedGame();
                }

                $this->logger->debug(
                    'Game debug info.',
                    $game->toArray()
                );

                /* Связываем игру из game_buffer с игрой из game */
                $gameBuffer->setGame($game);
                $this->entityManager->persist($gameBuffer);
                $this->entityManager->flush();
            }
        }

        return $result;
    }
}
