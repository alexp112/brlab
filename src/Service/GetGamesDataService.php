<?php

namespace App\Service;

use App\DTO\GameDTO;
use App\DTO\GamesDataDTO;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Сервис получения данных об играх от поставщиков данных
 */
class GetGamesDataService
{
    /**
     * Клиент для отправки HTTP запросов
     *
     * @var ClientInterface
     */
    private $client;
    /**
     * Служба логирвания
     *
     * @var LoggerInterface
     */
    private $logger;
    /**
     * URL провайдера данных
     *
     * @var string
     */
    private $providerUrl;

    public function __construct(
        ClientInterface $client,
        LoggerInterface $logger,
        string $providerUrl
    ) {
        $this->client = $client;
        $this->logger = $logger;
        $this->providerUrl = $providerUrl;
    }


    public function getData(): ?GamesDataDTO
    {
        try {
            $result = $this->client->request('GET', $this->providerUrl);
        } catch (GuzzleException $e) {
            $this->logger->error(
                sprintf(
                    'Failed to get data from provider. URL: "%s". %s.',
                    $this->providerUrl,
                    $e->getMessage()
                ),
                [
                    'stackTrace' => $e->getTraceAsString()
                ]
            );
            return null;
        }

        if ($result->getStatusCode() !== 200) {
            $this->logger->error(
                sprintf(
                    'Failed to get data from provider. URL: "%s". Status code: "%s"',
                    $this->providerUrl,
                    $result->getStatusCode()
                )
            );
        }

        $body = (string) $result->getBody();
        $bodyDecoded = json_decode($body, true);

        if ($bodyDecoded === false) {
            $this->logger->error(
                sprintf(
                    'Failed to decode response from provider. Response body: "%s"',
                    $body
                )
            );
            return null;
        }

        $gamesDataDTO = new GamesDataDTO();
        $gamesDataDTO->setSource($bodyDecoded['source'] ?? '');
        $gamesDataDTO->setLanguage($bodyDecoded['language'] ?? '');
        $gamesDataDTO->setSportType($bodyDecoded['sportType'] ?? '');
        $gamesDataDTO->setLeague($bodyDecoded['league'] ?? '');

        if (isset($bodyDecoded['games']) && is_array($bodyDecoded['games'])) {
            foreach ($bodyDecoded['games'] as $game) {
                if (isset($game['date'], $game['team1'], $game['team2']))
                {
                    $gameDTO = new GameDTO(
                        \DateTime::createFromFormat('Y.m.d H:i:s', $game['date']),
                        $game['team1'],
                        $game['team2']
                    );
                    $gamesDataDTO->addGame($gameDTO);
                }
            }
        }

        return $gamesDataDTO;
    }
}