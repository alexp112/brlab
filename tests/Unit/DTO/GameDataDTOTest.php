<?php

use App\DTO\GameDTO;
use App\DTO\GamesDataDTO;
use App\Entity\Game;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DTO\GamesDataDTO
 */
class GameDataDTOTest extends TestCase
{
    /**
     * Проверяет создание объекта с корректными параметрами
     */
    public function testInitObjectWithAllParams()
    {
        $gameDataDTO = new GamesDataDTO();
        $gameDataDTO->setSource("test source ");
        $this->assertEquals("test source", $gameDataDTO->getSource());

        $gameDataDTO->setLanguage("test language ");
        $this->assertEquals("test language", $gameDataDTO->getLanguage());

        $gameDataDTO->setSportType("test sportType ");
        $this->assertEquals("test sportType", $gameDataDTO->getSportType());

        $gameDataDTO->setLeague("test league ");
        $this->assertEquals("test league", $gameDataDTO->getLeague());

        $game1 = new GameDTO(new \DateTime(), 'team1', 'team2');
        $game2 = new GameDTO(new \DateTime(), 'team3', 'team4');
        $game3 = new GameDTO(new \DateTime(), 'team5', 'team6');
        $gameDataDTO->setGames([
            $game1,
            $game2
        ]);
        $this->assertEquals([
            $game1,
            $game2
        ], $gameDataDTO->getGames());
        $gameDataDTO->addGame($game3);
        $this->assertEquals([
            $game1,
            $game2,
            $game3
        ], $gameDataDTO->getGames());
    }

    /**
     * Проверяет создание объекта с некорректными параметрами
     */
    public function testInitObjectWrongParams()
    {
        // TODO: реализовать
        $this->assertTrue(true);
    }
}