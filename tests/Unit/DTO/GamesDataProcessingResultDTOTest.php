<?php

use App\DTO\GamesDataProcessingResultDTO;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DTO\GamesDataProcessingResultDTO
 */
class GamesDataProcessingResultDTOTest extends TestCase
{
    /**
     * Проверяет создание объекта с корректными параметрами
     */
    public function testInitObjectWithAllParams()
    {
        $gamesDataProcessingResultDTO = new GamesDataProcessingResultDTO();
        $gamesDataProcessingResultDTO->setGamesProcessedNum(2);
        $this->assertEquals(2, $gamesDataProcessingResultDTO->getGamesProcessedNum());
        $gamesDataProcessingResultDTO->setBufferGamesInsertedNum(20);
        $this->assertEquals(20,  $gamesDataProcessingResultDTO->getBufferGamesInsertedNum());
        $gamesDataProcessingResultDTO->setGamesInsertedNum(30);
        $this->assertEquals(30, $gamesDataProcessingResultDTO->getGamesInsertedNum());

        $gamesDataProcessingResultDTO->incrementOneProcessedGame();
        $this->assertEquals(3, $gamesDataProcessingResultDTO->getGamesProcessedNum());
        $gamesDataProcessingResultDTO->incrementOneInsertedBufferGame();
        $this->assertEquals(21,  $gamesDataProcessingResultDTO->getBufferGamesInsertedNum());
        $gamesDataProcessingResultDTO->incrementOneInsertedGame();
        $this->assertEquals(31, $gamesDataProcessingResultDTO->getGamesInsertedNum());
    }

    /**
     * Проверяет создание объекта с некорректными параметрами
     */
    public function testInitObjectWrongParams()
    {
        // TODO: реализовать
        $this->assertTrue(true);
    }
}