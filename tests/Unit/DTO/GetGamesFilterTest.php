<?php

use App\DTO\GetGamesFilter;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DTO\GetGamesFilter
 */
class GetGamesFilterTest extends TestCase
{
    /**
     * Проверяет создание объекта с корректными параметрами
     */
    public function testInitObjectWithAllParams()
    {
        $getGamesFilter = new GetGamesFilter(
            'testSource',
            '2019-02-24 12:00:00',
            '2019-02-25 12:00:00'
        );
        $this->assertEquals('testSource', $getGamesFilter->getSource());
        $this->assertEquals(
            '2019-02-24 12:00:00',
            $getGamesFilter->getMatchStartAtFrom()->format('Y-m-d H:i:s')
        );
        $this->assertEquals(
            '2019-02-25 12:00:00',
            $getGamesFilter->getMatchStartAtTo()->format('Y-m-d H:i:s')
        );
    }

    /**
     * Проверяет создание объекта не со всеми параметрами
     */
    public function testInitObjectNotAllParams()
    {
        // TODO: реализовать
        $this->assertTrue(true);
    }

    /**
     * Проверяет создание объекта с неверными датами
     */
    public function testInitObjectInvalidDate()
    {
        // TODO: реализовать
        $this->assertTrue(true);
    }
}