<?php

use App\DTO\GameDTO;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DTO\GameDTO
 */
class GameDTOTest extends TestCase
{
    /**
     * Проверяет создание объекта с корректными параметрами
     */
    public function testInitObjectWithAllParams()
    {
        $date = new \DateTime();
        $gameDTO = new GameDTO(
            $date,
            'Команда 1',
            'Команда 2'
        );
        $this->assertEquals($date, $gameDTO->getDate());
        $this->assertEquals('Команда 1', $gameDTO->getTeam1());
        $this->assertEquals('Команда 2', $gameDTO->getTeam2());
    }

    /**
     * Проверяет создание объекта с некорректными параметрами
     */
    public function testInitObjectWrongParams()
    {
        // TODO: реализовать
        $this->assertTrue(true);
    }
}