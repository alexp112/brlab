<?php

$sources = [
    'sportdata.com',
    'anotherdata.com'
];
$languages = [
    'русский',
    'ru',
    'en',
    'eng'
];
$sportTypes = [
    'футбол',
    'futbol',
    'football'
];
$leagues = [
    'Лига чемпионов УЕФА',
    'Liga UEFA',
    'League UEFA'
];
$teams = [
    [
        'Реал',
        'Реал Мадрид',
        'Real',
        'Real M'
    ],
    [
        'Барселона',
        'ФК Барселона',
        'Barcelona'
    ]
];

function getRandomSource()
{
    global $sources;
    return $sources[array_rand($sources)];
}
function getRandomLanguage()
{
    global $languages;
    return $languages[array_rand($languages)];
}
function getRandomSportType()
{
    global $sportTypes;
    return $sportTypes[array_rand($sportTypes)];
}
function getRandomLeague()
{
    global $leagues;
    return $leagues[array_rand($leagues)];
}
function getRandom2Teams()
{
    global $teams;
    $team1Index = array_rand($teams);
    $team1 = $teams[$team1Index];
    do {
        $team2Index = array_rand($teams);
    } while ($team2Index == $team1Index);
    $team2 = $teams[$team2Index];
    return [$team1[array_rand($team1)], $team2[array_rand($team2)]];
}
function getRandomDate(): \DateTime
{
    $date = new \DateTime();
    $date->modify('+' . rand(1, 30) . ' days');
    $date->setTime(rand(0, 23), rand(0, 59), rand(0, 59));
    return $date;
}

$source = getRandomSource();
$language = getRandomLanguage();
$sportType = getRandomSportType();
$league = getRandomLeague();
$data = [
    'source' => $source,
    'language' => $language,
    'sportType' => $sportType,
    'league' => $league,
    'games' => []
];
for ($i = 0; $i < 10; $i++) {
    $gameTeams = getRandom2Teams();
    $data['games'][] = [
        'date' => getRandomDate()->format('Y.m.d H:i:s'),
        'team1' => $gameTeams[0],
        'team2' => $gameTeams[1]
    ];
}

header('Content-type:application/json;charset=utf-8');
echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);