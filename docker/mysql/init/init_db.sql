CREATE DATABASE brlabDb;
CREATE USER 'brlabUser'@'%' IDENTIFIED BY 'brlabPassword';
GRANT ALL ON brlabDb.* TO 'brlabUser'@'%';
USE brlabDb;

     CREATE TABLE league (id INT AUTO_INCREMENT NOT NULL, sport_type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_3EB4C31864F9C039 (sport_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE game_buffer (id INT AUTO_INCREMENT NOT NULL, language_id INT DEFAULT NULL, sport_type_id INT DEFAULT NULL, league_id INT DEFAULT NULL, team1_id INT DEFAULT NULL, team2_id INT DEFAULT NULL, source_id INT NOT NULL, game_id INT NULL, match_start_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_4C6F5D3A82F1BAF4 (language_id), INDEX IDX_4C6F5D3A64F9C039 (sport_type_id), INDEX IDX_4C6F5D3A58AFC4DE (league_id), INDEX IDX_4C6F5D3AE72BCFA4 (team1_id), INDEX IDX_4C6F5D3AF59E604A (team2_id), INDEX IDX_4C6F5D3A953C1C61 (source_id), INDEX IDX_4C6F5D3AE48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE league_dictionary (id INT AUTO_INCREMENT NOT NULL, league_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_7E9955AC58AFC4DE (league_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE language_dictionary (id INT AUTO_INCREMENT NOT NULL, language_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_5FB98E3B82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE sport_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE source (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, sport_type_id INT NOT NULL, league_id INT NOT NULL, team1_id INT NOT NULL, team2_id INT NOT NULL, match_start_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_232B318C82F1BAF4 (language_id), INDEX IDX_232B318C64F9C039 (sport_type_id), INDEX IDX_232B318C58AFC4DE (league_id), INDEX IDX_232B318CE72BCFA4 (team1_id), INDEX IDX_232B318CF59E604A (team2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, sport_type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_C4E0A61F64F9C039 (sport_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE sport_type_dictionary (id INT AUTO_INCREMENT NOT NULL, sport_type_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_BDC5E3AA64F9C039 (sport_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE team_dictionary (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_217997E3296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
     ALTER TABLE league ADD CONSTRAINT FK_3EB4C31864F9C039 FOREIGN KEY (sport_type_id) REFERENCES sport_type (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3A82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3A64F9C039 FOREIGN KEY (sport_type_id) REFERENCES sport_type (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3A58AFC4DE FOREIGN KEY (league_id) REFERENCES league (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3AE72BCFA4 FOREIGN KEY (team1_id) REFERENCES team (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3AF59E604A FOREIGN KEY (team2_id) REFERENCES team (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3A953C1C61 FOREIGN KEY (source_id) REFERENCES source (id);
     ALTER TABLE game_buffer ADD CONSTRAINT FK_4C6F5D3AE48FD905 FOREIGN KEY (game_id) REFERENCES game (id);
     ALTER TABLE league_dictionary ADD CONSTRAINT FK_7E9955AC58AFC4DE FOREIGN KEY (league_id) REFERENCES league (id);
     ALTER TABLE language_dictionary ADD CONSTRAINT FK_5FB98E3B82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id);
     ALTER TABLE game ADD CONSTRAINT FK_232B318C82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id);
     ALTER TABLE game ADD CONSTRAINT FK_232B318C64F9C039 FOREIGN KEY (sport_type_id) REFERENCES sport_type (id);
     ALTER TABLE game ADD CONSTRAINT FK_232B318C58AFC4DE FOREIGN KEY (league_id) REFERENCES league (id);
     ALTER TABLE game ADD CONSTRAINT FK_232B318CE72BCFA4 FOREIGN KEY (team1_id) REFERENCES team (id);
     ALTER TABLE game ADD CONSTRAINT FK_232B318CF59E604A FOREIGN KEY (team2_id) REFERENCES team (id);
     ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F64F9C039 FOREIGN KEY (sport_type_id) REFERENCES sport_type (id);
     ALTER TABLE sport_type_dictionary ADD CONSTRAINT FK_BDC5E3AA64F9C039 FOREIGN KEY (sport_type_id) REFERENCES sport_type (id);
     ALTER TABLE team_dictionary ADD CONSTRAINT FK_217997E3296CD8AE FOREIGN KEY (team_id) REFERENCES team (id);

ALTER TABLE `game` ADD INDEX `IDX_match_start_at` (`match_start_at`);
ALTER TABLE `game_buffer` ADD INDEX `IDX_match_start_at` (`match_start_at`);

INSERT INTO `language` (`name`, `code`) VALUES ('Русский', 'ru');
INSERT INTO `language` (`name`, `code`) VALUES ('Английский', 'en');
INSERT INTO `language_dictionary` (`language_id`, `value`) VALUES ('1', 'русский');
INSERT INTO `language_dictionary` (`language_id`, `value`) VALUES ('1', 'ru');
INSERT INTO `language_dictionary` (`language_id`, `value`) VALUES ('2', 'en');
INSERT INTO `language_dictionary` (`language_id`, `value`) VALUES ('2', 'eng');

INSERT INTO `sport_type` (`name`) VALUES ('футбол');

INSERT INTO `league` (`sport_type_id`, `name`) VALUES ('1', 'Лига чемпионов УЕФА');

INSERT INTO `league_dictionary` (`league_id`, `value`) VALUES ('1', 'Лига чемпионов УЕФА');
INSERT INTO `league_dictionary` (`league_id`, `value`) VALUES ('1', 'Liga UEFA');
INSERT INTO `league_dictionary` (`league_id`, `value`) VALUES ('1', 'League UEFA');

INSERT INTO `team` (`sport_type_id`, `name`) VALUES ('1', 'Реал');
INSERT INTO `team` (`sport_type_id`, `name`) VALUES ('1', 'Барселона');

INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('1', 'Реал');
INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('1', 'Реал Мадрид');
INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('1', 'Real');
INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('1', 'Real M');
INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('2', 'Барселона');
INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('2', 'ФК Барселона');
INSERT INTO `team_dictionary` (`team_id`, `value`) VALUES ('2', 'Barcelona');

INSERT INTO `sport_type_dictionary` (`sport_type_id`, `value`) VALUES ('1', 'футбол');
INSERT INTO `sport_type_dictionary` (`sport_type_id`, `value`) VALUES ('1', 'futbol');
INSERT INTO `sport_type_dictionary` (`sport_type_id`, `value`) VALUES ('1', 'football');

INSERT INTO `source` (`name`) VALUES ('sportdata.com');
INSERT INTO `source` (`name`) VALUES ('anotherdata.com');