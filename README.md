# BRLab

## Тестовое задание для BRLab
Описание тестового задания: https://docs.google.com/document/d/1RhVlrc76FDsrXjJyJQZu9SJp4o3b2cARz-ah4xmJAVM/edit

## Запуск приложения
Для запуска приложения необходимо сделать clone репозитория и в папке проекта выполнить ```docker-compose up -d```

## REST API приложения

### Получение игр
Метод для получения списка игр доступен по адресу [http://localhost:8082/api/games](http://localhost:8082/api/games)

#### GET параметры фильтрации и сортировки

* source - название источника данных
* limit - количество возвращаемых игр
* offset - смещение первой возвращаемой игры
* match_start_at_from - начало интервала даты матча
* match_start_at_to - конец интервала даты матча

#### Примеры запроса 

* [http://localhost:8082/api/games?match_start_at_from=2019-11-13 08:28:31&match_start_at_to=2019-11-14 23:00:00&source=sportdata&limit=2&offset=1](http://localhost:8082/api/games?match_start_at_from=2019-11-13%2008:28:31&match_start_at_to=2019-11-14%2023:00:00&source=sportdata&limit=2&offset=1)
* [http://localhost:8082/api/games?source=sportdata&limit=5](http://localhost:8082/api/games?source=sportdata&limit=5)

## Получение данных об играх

Получение данных об играх происходит по cron, раз в минуту.

Демо данные об играх берутся по адресу: [http://localhost:8084/](http://localhost:8084/)

Для запуска команды для получения данных в ручную необходимо выполнить внутри контейнера: ```/app/bin/console app:get-data```

## Демо данные

Демо данные об играх доступны по адресу: [http://localhost:8084/](http://localhost:8084/)

## Adminer

Adminer доступен по адресу: [http://localhost:8083/](http://localhost:8083/)

Данные для доступа:
```
Сервер: mysql
Имя пользователя: brlabUser
Пароль: brlabPassword
База данных: brlabDb
```